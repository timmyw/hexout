;;;   -*- mode: asm -*-

;;; 7-segment font

;;; E10561GBL0W - common anode
;;;
;;; Q:S:P
;;; 0:A: 7            --A--
;;; 1:B: 6           |     |
;;; 2:C: 4           F     B
;;; 3:D: 2           |     |
;;; 4:E: 1            --G--
;;; 5:F: 9           |     |
;;; 6:G:10           E     C
;;;                  |     |
;;;                   --D--
	
.equ FN_0 =  0b11000000		; abcdef--
.equ FN_1 =  0b11111001		; -bc-----
.equ FN_2 =  0b10100100		; abdeg---
.equ FN_3 =  0b10110000		; abcd__g_
.equ FN_4 =  0b10011001		; _bc__fg_
.equ FN_5 =  0b10010010		; a_cd_fg_
.equ FN_6 =  0b10000010		; a_cdefg_
.equ FN_7 =  0b11111000		; abc_____
.equ FN_8 =  0b10000000		; abcdefg_
.equ FN_9 =  0b10010000		; abcd_fg_
.equ FN_A =  0b10001000		; abc_efg_
.equ FN_B =  0b10000011		; --cdefg-
.equ FN_C =  0b11000110		; a__def__
.equ FN_D =  0b10100001		; -bcde-g-
.equ FN_E =  0b10000110		; a--defg-
.equ FN_F =  0b10001110		; a---efg-

