.nolist
.include "./tn85def.inc"
.list
	
.def temp = r16
	
.equ LATCH_OUT = DDB0		; OUT
.equ SER_OUT   = DDB1		; OUT
.equ CLOCK     = DDB2		; OUT
.equ SER_IN    = DDB3		; IN
.equ LATCH_IN  = DDB4		; OUT

.org 0x0000
	rjmp main

.org 0x020	
main:
	;; Set up the port directions
 	ldi temp, (1<<LATCH_IN) | (1<<CLOCK) | (1<<SER_OUT) | (1<<LATCH_OUT)
 	out DDRB, temp
	;; Set the LATCH_IN high
	sbi PortB, LATCH_IN
	rcall init_data
	
	rcall serial_read_16
	;; We should have bytes in r17 and r18

	;; mov r16, r18
	rcall seven_write_bytes

	;; Dump both bytes to SER_OUT
	;; mov r16, r18
	;; rcall serial_write_8
	;; mov r16, r17
	;; rcall serial_write_8
	;; rcall delay_10_ms	
	rjmp main

.include "./delay.asm"

seven_font_list:
	.db FN_0, FN_1, FN_2, FN_3, FN_4, FN_5, FN_6, FN_7
	.db FN_8, FN_9, FN_A, FN_B, FN_C, FN_D, FN_E, FN_F

init_data:
	;; Copy the initial values for the 7-segment font from
	;; Flash into RAM.
	ldi ZH, high(seven_font_list << 1)
	ldi ZL, low(seven_font_list << 1)
	ldi XH, high(seven_font)
	ldi XL, low(seven_font)
	ldi r16, 16		; There are 16 characters
init_data_loop:
	lpm r17, Z+	
	st X+, r17
	dec r16
	brne init_data_loop
	ret
	
;;; Write the bytes in r17 and r18 to the output
;;; Retrieve the 4 font values, and store them in ~four_nibbles~:
;;; four_nibbles+0: byte 1: high
;;; four_nibbles+1: byte 1: low
;;; four_nibbles+2: byte 2: high
;;; four_nibbles+3: byte 2: low
seven_write_bytes:
	mov r16, r17
	;; Low nibble of byte 1
	andi r16, $0f
	ldi xh, high(seven_font)
	ldi xl, low(seven_font)
	add xl, r16
	ld  r16, x
	sts four_nibbles, r16
	mov r16, r17
	swap r16
	andi r16, $0f
	ldi xl, low(seven_font)
	add xl, r16
	ld  r16, x
	sts four_nibbles+1, r16


	mov r16, r18
	;; Low nibble of byte 1
	andi r16, $0f
	ldi xh, high(seven_font)
	ldi xl, low(seven_font)
	add xl, r16
	ld  r16, x
	sts four_nibbles+2, r16
	mov r16, r18
	swap r16
	andi r16, $0f
	ldi xl, low(seven_font)
	add xl, r16
	ld  r16, x
	sts four_nibbles+3, r16

	;; Hard code some test values
 	;; ldi r16, FN_6
	;; sts four_nibbles, r16
	;; ldi r16, FN_A
	;; sts four_nibbles+1, r16
	
	rcall serial_write_32
	ret
	
;;; Write the lower order nibble in r16 to the output
;; seven_write_nibble:
;; 	andi r16, $0f
;; 	ldi xh, high(seven_font)
;; 	ldi xl, low(seven_font)
;; 	add xl, r16
;; 	ld  r16, x		; r16 should have the font bitmap
;; 	rcall serial_write_8
;; 	ret

;;; Write 32 bits to SER_OUT
;;; Expects the 32 bits in 4 bytes starting at four_nibbles
;;; Clobbers: r18, r19 (and anything that the delay loop clobbers)
serial_write_32:
	ldi r20, 0		; Initial offset
	ldi xh, high(four_nibbles)
sw_32_outer:
	ldi xl, low(four_nibbles)
	add xl, r20
	ld r19, x
	ldi r18, 8		; 8 bits at a time
sw_32_loop:			; Start writing the current byte
	rol r19
	brcc sw_32_zero
sw_32_one:
	sbi PortB, SER_OUT
	rjmp sw_32_done
sw_32_zero:
	cbi PortB, SER_OUT	
	rjmp sw_32_done
sw_32_done:
	sbi PortB, CLOCK
	rcall delay_10_ms
	cbi PortB, CLOCK
	rcall delay_10_ms
 	dec r18
	brne sw_32_loop

	;; Next byte to send
	inc r20
	cpi r20, 4		; Have we done all the bytes
	brne sw_32_outer
	
	cbi PortB, SER_OUT

	;;  Pulse the LATCH_OUT pin
	rcall pulse_latch_out
	ret
	

;;; Read in 16 bits from SER_IN.  Uses CLOCK and LATCH_IN
;;; Destroys r16, r17, r18, r19 and r20
;;; The result are in: byte0:r18 and byte1:r17
serial_read_16:

	;; Pulse the latch signal low then high.
	rcall pulse_latch_in

	ldi r18, 0		; This will store the first byte
	;; ldi r20, 1		;We're doing two bytes
	ldi r19, 16 		; 8 bits
	ldi r17, 0		; Clear our building byte

	;; At this point
	;; Q_H should have the first bit
sr_16_loop:
	;; Read SER_IN
	in r16, PinB
	andi r16, (1<<SER_IN)
	;; Build r17
	brne sr_16_one

sr_16_zero: clc
	;; cbi PortB, SER_OUT	; TODO
	rjmp done_16_read

sr_16_one: sec
	;; sbi PortB, SER_OUT	; TODO
	rjmp done_16_read	; Keep the timings the same

done_16_read:
	;; Rotate r17 for the next bit, pulling in C
	rol r17
	;; Now shift the bits
	;; Pulse clock pin
	sbi PortB, CLOCK
	rcall delay_10_ms
	cbi PortB, CLOCK

	;; Dec the loop and jump back
	rcall delay_10_ms
	;; cbi PortB, SER_OUT	; TODO

	;; Shift to next byte if necessary
	cpi r19, 9
	brne sr_16_same_byte

	;; We've finished byte one
	mov r18, r17		; Store the first byte
sr_16_same_byte:	
	dec r19
	brne sr_16_loop
	
	;; We have the first byte in r18 and the second in r17
	ret
	
;;; Pulse the latch pin low and then high again
pulse_latch_in:
	;; Set DDB5 pin low
	cbi PortB, LATCH_IN
	;; Delay
	rcall delay_20_ms
	;; Set pin high
	sbi PortB, LATCH_IN
	ret

;;; Pulse the latch pin low and then high again
pulse_latch_out:
	;; Set DDB5 pin low
	cbi PortB, LATCH_OUT
	;; Delay
	rcall delay_10_ms
	;; Set pin high
	sbi PortB, LATCH_OUT
	ret
	
pulse_clock_up:
	;; Set pin high
	sbi PortB, CLOCK
	ret

pulse_clock_down:
	;; Set pin high
	cbi PortB, CLOCK
	ret
	
;;; Delay for (approx) 20 milliseconds
delay_20_ms:			; 20ms is 800000 cycles
	ldi temp, 2		; 20 x 1 ms
outer_loop_d20:
 	ldi r24, low(63484)	; Count from 63484 up to 65535
	ldi r25, high(63484)	; which is 2051
delay_loop_d20:			;
	adiw r24, 1 		;
	brne delay_loop_d20	; 
	dec temp
	brne outer_loop_d20
	ret	

;;; Delay for (approx) 10 milliseconds
delay_10_ms:			; 10ms is 400000 cycles
	ldi temp, 1		; 
outer_loop_d10:
 	ldi r24, low(63484)	; Count from 63484 up to 65535
	ldi r25, high(63484)	; which is 2051
delay_loop_d10:			; 4 cycles for the loop
	adiw r24, 1 		;
	brne delay_loop_d10	; 
	;; If we get here we've done 2051*4=8204(ish) cycles
	dec temp
	brne outer_loop_d10
	ret	
	
;;; M=C/t
;;; 16000 cycles is 1 millisecond
;;; Inner loop is 3 cycles, so loop
;;; 16000/3 =  5333 times
;;; 65535-5333 = 60202

.include "./7segment.inc"

.dseg
seven_font:	.byte 	$10 

four_nibbles:	.byte $04	; Store the four fonts for writing
