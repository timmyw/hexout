
PORT=/dev/ttyACM1
MCU=t85
PRGMR=avrisp
BITRATE=19200
AVRDUDEFLAGS=-v -e -D -p ${MCU} -c ${PRGMR} -b ${BITRATE} -P ${PORT}

%.hex: %.asm
	avra $<

hexout.hex: hexout.asm 7segment.inc

hexout: hexout.hex
	avrdude ${AVRDUDEFLAGS} -U flash:w:$<
