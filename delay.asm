delay_05:
	ldi temp, 8
outer_loop:
 
	;; ldi r24, low(3037)
	;; ldi r25, high(3037)
	ldi r24, low($0)
	ldi r25, high($0)
delay_loop:
	adiw r24, 1
	brne delay_loop
 
	dec temp
	brne outer_loop
	ret	
