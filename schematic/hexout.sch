EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Hexout"
Date ""
Rev ""
Comp ""
Comment1 "Author: Tim Whelan"
Comment2 "https://gitlab.com/timmyw/avr/-/tree/master/hexout"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5ED18A0B
P 10500 1050
F 0 "#FLG02" H 10500 1125 50  0001 C CNN
F 1 "PWR_FLAG" H 10500 1223 50  0000 C CNN
F 2 "" H 10500 1050 50  0001 C CNN
F 3 "~" H 10500 1050 50  0001 C CNN
	1    10500 1050
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5ED18DF3
P 9800 1050
F 0 "#FLG01" H 9800 1125 50  0001 C CNN
F 1 "PWR_FLAG" H 9800 1223 50  0000 C CNN
F 2 "" H 9800 1050 50  0001 C CNN
F 3 "~" H 9800 1050 50  0001 C CNN
	1    9800 1050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5ED19AE0
P 10500 1050
F 0 "#PWR010" H 10500 800 50  0001 C CNN
F 1 "GND" H 10505 877 50  0000 C CNN
F 2 "" H 10500 1050 50  0001 C CNN
F 3 "" H 10500 1050 50  0001 C CNN
	1    10500 1050
	1    0    0    -1  
$EndComp
Text Label 1750 1500 0    50   ~ 0
LATCH_OUT
Text Label 1750 1400 0    50   ~ 0
SER_OUT
Text Label 1750 1300 0    50   ~ 0
CLOCK
Text Label 1750 1200 0    50   ~ 0
SER_IN
Text Label 1750 1100 0    50   ~ 0
LATCH_IN
Wire Wire Line
	1600 1100 1750 1100
Wire Wire Line
	1600 1200 1750 1200
Wire Wire Line
	1750 1300 1600 1300
Wire Wire Line
	1600 1400 1750 1400
Wire Wire Line
	1750 1500 1600 1500
$Comp
L power:GND #PWR02
U 1 1 5ED1AEE4
P 1300 800
F 0 "#PWR02" H 1300 550 50  0001 C CNN
F 1 "GND" H 1305 627 50  0000 C CNN
F 2 "" H 1300 800 50  0001 C CNN
F 3 "" H 1300 800 50  0001 C CNN
	1    1300 800 
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR01
U 1 1 5ED1B2AD
P 1300 1700
F 0 "#PWR01" H 1300 1550 50  0001 C CNN
F 1 "VCC" H 1317 1873 50  0000 C CNN
F 2 "" H 1300 1700 50  0001 C CNN
F 3 "" H 1300 1700 50  0001 C CNN
	1    1300 1700
	-1   0    0    1   
$EndComp
NoConn ~ 1600 1000
$Comp
L Connector_Generic:Conn_01x08 J1
U 1 1 5ED1F010
P 1300 3300
F 0 "J1" H 1300 3700 50  0000 C CNN
F 1 "Conn_01x08" H 1218 2766 50  0001 C CNN
F 2 "" H 1300 3300 50  0001 C CNN
F 3 "~" H 1300 3300 50  0001 C CNN
	1    1300 3300
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5ED2033C
P 2650 4450
F 0 "#PWR05" H 2650 4200 50  0001 C CNN
F 1 "GND" H 2655 4277 50  0000 C CNN
F 2 "" H 2650 4450 50  0001 C CNN
F 3 "" H 2650 4450 50  0001 C CNN
	1    2650 4450
	1    0    0    -1  
$EndComp
Text Label 2700 4000 2    50   ~ 0
CLOCK
Text Label 2700 3800 2    50   ~ 0
LATCH_IN
$Comp
L power:GND #PWR03
U 1 1 5ED367D1
P 1250 2200
F 0 "#PWR03" H 1250 1950 50  0001 C CNN
F 1 "GND" H 1255 2027 50  0000 C CNN
F 2 "" H 1250 2200 50  0001 C CNN
F 3 "" H 1250 2200 50  0001 C CNN
	1    1250 2200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J2
U 1 1 5ED66E95
P 1350 5750
F 0 "J2" H 1350 6150 50  0000 C CNN
F 1 "Conn_01x08" H 1268 5216 50  0001 C CNN
F 2 "" H 1350 5750 50  0001 C CNN
F 3 "~" H 1350 5750 50  0001 C CNN
	1    1350 5750
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5ED66EA0
P 3350 6900
F 0 "#PWR08" H 3350 6650 50  0001 C CNN
F 1 "GND" H 3355 6727 50  0000 C CNN
F 2 "" H 3350 6900 50  0001 C CNN
F 3 "" H 3350 6900 50  0001 C CNN
	1    3350 6900
	1    0    0    -1  
$EndComp
Text Label 2700 6450 2    50   ~ 0
CLOCK
Text Label 2700 6250 2    50   ~ 0
LATCH_IN
Connection ~ 3350 6900
$Comp
L power:GND #PWR04
U 1 1 5ED66F3C
P 1250 4650
F 0 "#PWR04" H 1250 4400 50  0001 C CNN
F 1 "GND" H 1255 4477 50  0000 C CNN
F 2 "" H 1250 4650 50  0001 C CNN
F 3 "" H 1250 4650 50  0001 C CNN
	1    1250 4650
	1    0    0    -1  
$EndComp
Text Label 2700 2800 2    50   ~ 0
HIGH_SER
Text Label 3950 2800 0    50   ~ 0
SER_IN
Text Label 4000 5250 0    50   ~ 0
HIGH_SER
$Comp
L power:VCC #PWR09
U 1 1 5EDA7657
P 9800 1250
F 0 "#PWR09" H 9800 1100 50  0001 C CNN
F 1 "VCC" H 9818 1423 50  0000 C CNN
F 2 "" H 9800 1250 50  0001 C CNN
F 3 "" H 9800 1250 50  0001 C CNN
	1    9800 1250
	-1   0    0    1   
$EndComp
Wire Wire Line
	9800 1250 9800 1050
$Comp
L dk_Embedded-Microcontrollers:ATTINY85-20PU U1
U 1 1 5ED16749
P 1400 1300
F 0 "U1" H 1300 850 60  0000 L CNN
F 1 "ATTINY85-20PU" V 1650 900 60  0000 L CNN
F 2 "digikey-footprints:DIP-8_W7.62mm" H 1600 1500 60  0001 L CNN
F 3 "http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en589894" H 1600 1600 60  0001 L CNN
F 4 "ATTINY85-20PU-ND" H 1600 1700 60  0001 L CNN "Digi-Key_PN"
F 5 "ATTINY85-20PU" H 1600 1800 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 1600 1900 60  0001 L CNN "Category"
F 7 "Embedded - Microcontrollers" H 1600 2000 60  0001 L CNN "Family"
F 8 "http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en589894" H 1600 2100 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/microchip-technology/ATTINY85-20PU/ATTINY85-20PU-ND/735469" H 1600 2200 60  0001 L CNN "DK_Detail_Page"
F 10 "IC MCU 8BIT 8KB FLASH 8DIP" H 1600 2300 60  0001 L CNN "Description"
F 11 "Microchip Technology" H 1600 2400 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1600 2500 60  0001 L CNN "Status"
	1    1400 1300
	-1   0    0    1   
$EndComp
$Comp
L 74xx:74LS165 U2
U 1 1 5EDCCA5E
P 3300 3400
F 0 "U2" H 3050 2550 50  0000 C CNN
F 1 "74HC165" V 3650 2800 50  0000 C CNN
F 2 "" H 3300 3400 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS165" H 3300 3400 50  0001 C CNN
	1    3300 3400
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR06
U 1 1 5ED2132C
P 3300 2500
F 0 "#PWR06" H 3300 2350 50  0001 C CNN
F 1 "VCC" H 3317 2673 50  0000 C CNN
F 2 "" H 3300 2500 50  0001 C CNN
F 3 "" H 3300 2500 50  0001 C CNN
	1    3300 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 2800 3800 2800
NoConn ~ 3800 2900
Wire Wire Line
	2800 2800 2700 2800
Wire Wire Line
	2700 4000 2800 4000
Wire Wire Line
	2800 4100 2650 4100
Wire Wire Line
	3300 4400 2650 4400
Connection ~ 2650 4400
Wire Wire Line
	2650 4400 2650 4450
Wire Wire Line
	2800 3800 2700 3800
$Comp
L 74xx:74LS165 U3
U 1 1 5EE5FF87
P 3350 5850
F 0 "U3" H 3100 5000 50  0000 C CNN
F 1 "74HC165" V 3700 5250 50  0000 C CNN
F 2 "" H 3350 5850 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS165" H 3350 5850 50  0001 C CNN
	1    3350 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 6850 3350 6900
$Comp
L power:VCC #PWR07
U 1 1 5ED66EAA
P 3350 4950
F 0 "#PWR07" H 3350 4800 50  0001 C CNN
F 1 "VCC" H 3367 5123 50  0000 C CNN
F 2 "" H 3350 4950 50  0001 C CNN
F 3 "" H 3350 4950 50  0001 C CNN
	1    3350 4950
	1    0    0    -1  
$EndComp
NoConn ~ 3850 5350
Wire Wire Line
	3850 5250 4000 5250
Wire Wire Line
	2850 6250 2700 6250
Wire Wire Line
	2850 6450 2700 6450
Wire Wire Line
	2850 6900 3350 6900
Wire Wire Line
	2850 6550 2850 6900
Wire Wire Line
	2850 5250 2850 4800
$Comp
L Display_Character:KCSA02-136 U5
U 1 1 5EE99C03
P 7850 1350
F 0 "U5" V 8100 1800 50  0000 C CNN
F 1 "KCSA02-136" H 7850 1926 50  0001 C CNN
F 2 "Display_7Segment:KCSC02-136" H 7850 750 50  0001 C CNN
F 3 "http://www.kingbright.com/attachments/file/psearch/000/00/00/KCSA02-136(Ver.7B).pdf" H 7350 1825 50  0001 L CNN
	1    7850 1350
	1    0    0    -1  
$EndComp
$Comp
L dk_Logic-Shift-Registers:SN74HC595N U4
U 1 1 5EE9AF86
P 7050 1450
F 0 "U4" H 6600 900 60  0000 C CNN
F 1 "SN74HC595N" H 7150 2100 60  0000 C CNN
F 2 "digikey-footprints:DIP-16_W7.62mm" H 7250 1650 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74hc595" H 7250 1750 60  0001 L CNN
F 4 "296-1600-5-ND" H 7250 1850 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74HC595N" H 7250 1950 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 7250 2050 60  0001 L CNN "Category"
F 7 "Logic - Shift Registers" H 7250 2150 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74hc595" H 7250 2250 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74HC595N/296-1600-5-ND/277246" H 7250 2350 60  0001 L CNN "DK_Detail_Page"
F 10 "IC 8-BIT SHIFT REGISTER 16-DIP" H 7250 2450 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 7250 2550 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7250 2650 60  0001 L CNN "Status"
	1    7050 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 1050 7550 1050
Wire Wire Line
	7250 1150 7550 1150
Wire Wire Line
	7250 1350 7550 1350
Wire Wire Line
	7250 1450 7550 1450
Wire Wire Line
	7550 1550 7250 1550
NoConn ~ 7550 1750
NoConn ~ 7250 1750
$Comp
L power:VCC #PWR012
U 1 1 5EEE34D9
P 6850 700
F 0 "#PWR012" H 6850 550 50  0001 C CNN
F 1 "VCC" H 6867 873 50  0000 C CNN
F 2 "" H 6850 700 50  0001 C CNN
F 3 "" H 6850 700 50  0001 C CNN
	1    6850 700 
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 700  6850 750 
$Comp
L power:VCC #PWR013
U 1 1 5EEE9385
P 8350 1350
F 0 "#PWR013" H 8350 1200 50  0001 C CNN
F 1 "VCC" H 8367 1523 50  0000 C CNN
F 2 "" H 8350 1350 50  0001 C CNN
F 3 "" H 8350 1350 50  0001 C CNN
	1    8350 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R17
U 1 1 5EEE98B6
P 8350 1500
F 0 "R17" V 8350 1450 50  0000 L CNN
F 1 "180" V 8450 1400 50  0000 L CNN
F 2 "" V 8280 1500 50  0001 C CNN
F 3 "~" H 8350 1500 50  0001 C CNN
	1    8350 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 1650 8350 1750
Wire Wire Line
	8350 1750 8150 1750
Wire Wire Line
	8150 1650 8350 1650
Connection ~ 8350 1650
Wire Wire Line
	7250 1250 7550 1250
Wire Wire Line
	7250 1650 7550 1650
Text Label 5850 1150 2    50   ~ 0
SER_OUT
Wire Wire Line
	5850 1150 6450 1150
Wire Wire Line
	6450 1250 6300 1250
Wire Wire Line
	6300 1250 6300 2050
$Comp
L power:GND #PWR0101
U 1 1 5EF58AFF
P 6300 6550
F 0 "#PWR0101" H 6300 6300 50  0001 C CNN
F 1 "GND" H 6305 6377 50  0000 C CNN
F 2 "" H 6300 6550 50  0001 C CNN
F 3 "" H 6300 6550 50  0001 C CNN
	1    6300 6550
	1    0    0    -1  
$EndComp
Text Label 5850 1450 2    50   ~ 0
CLOCK
Wire Wire Line
	5850 1450 6450 1450
Wire Wire Line
	6450 1550 6100 1550
Wire Wire Line
	6100 1550 6100 700 
Wire Wire Line
	6100 700  6850 700 
Connection ~ 6850 700 
Text Label 5850 1350 2    50   ~ 0
LATCH_OUT
Wire Wire Line
	6450 1350 5850 1350
$Comp
L dk_Logic-Shift-Registers:SN74HC595N U6
U 1 1 5EF70373
P 7000 2900
F 0 "U6" H 6550 2350 60  0000 C CNN
F 1 "SN74HC595N" H 7100 3550 60  0000 C CNN
F 2 "digikey-footprints:DIP-16_W7.62mm" H 7200 3100 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74hc595" H 7200 3200 60  0001 L CNN
F 4 "296-1600-5-ND" H 7200 3300 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74HC595N" H 7200 3400 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 7200 3500 60  0001 L CNN "Category"
F 7 "Logic - Shift Registers" H 7200 3600 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74hc595" H 7200 3700 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74HC595N/296-1600-5-ND/277246" H 7200 3800 60  0001 L CNN "DK_Detail_Page"
F 10 "IC 8-BIT SHIFT REGISTER 16-DIP" H 7200 3900 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 7200 4000 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7200 4100 60  0001 L CNN "Status"
	1    7000 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 2050 6300 2050
Connection ~ 6300 2050
Wire Wire Line
	6300 2050 6300 2700
Wire Wire Line
	6800 2200 6100 2200
Wire Wire Line
	6100 2200 6100 1550
Connection ~ 6100 1550
Wire Wire Line
	7250 1850 7250 2150
Wire Wire Line
	7250 2150 6200 2150
Wire Wire Line
	6200 2150 6200 2600
Wire Wire Line
	6200 2600 6400 2600
$Comp
L Display_Character:KCSA02-136 U7
U 1 1 5EF80079
P 7800 2800
F 0 "U7" V 8050 3250 50  0000 C CNN
F 1 "KCSA02-136" H 7800 3376 50  0001 C CNN
F 2 "Display_7Segment:KCSC02-136" H 7800 2200 50  0001 C CNN
F 3 "http://www.kingbright.com/attachments/file/psearch/000/00/00/KCSA02-136(Ver.7B).pdf" H 7300 3275 50  0001 L CNN
	1    7800 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 2500 7500 2500
Wire Wire Line
	7200 2600 7500 2600
Wire Wire Line
	7200 2800 7500 2800
Wire Wire Line
	7200 2900 7500 2900
Wire Wire Line
	7500 3000 7200 3000
NoConn ~ 7500 3200
$Comp
L power:VCC #PWR011
U 1 1 5EF80089
P 8300 2800
F 0 "#PWR011" H 8300 2650 50  0001 C CNN
F 1 "VCC" H 8317 2973 50  0000 C CNN
F 2 "" H 8300 2800 50  0001 C CNN
F 3 "" H 8300 2800 50  0001 C CNN
	1    8300 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R18
U 1 1 5EF80093
P 8300 2950
F 0 "R18" V 8300 2900 50  0000 L CNN
F 1 "180" V 8400 2850 50  0000 L CNN
F 2 "" V 8230 2950 50  0001 C CNN
F 3 "~" H 8300 2950 50  0001 C CNN
	1    8300 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 3100 8300 3200
Wire Wire Line
	8300 3200 8100 3200
Wire Wire Line
	8100 3100 8300 3100
Connection ~ 8300 3100
Wire Wire Line
	7200 2700 7500 2700
Wire Wire Line
	7200 3100 7500 3100
Wire Wire Line
	6400 2700 6300 2700
Connection ~ 6300 2700
Wire Wire Line
	6300 2700 6300 3500
Text Label 5850 2800 2    50   ~ 0
LATCH_OUT
Wire Wire Line
	6400 2800 5850 2800
Text Label 5850 2900 2    50   ~ 0
CLOCK
Wire Wire Line
	6400 2900 5850 2900
Wire Wire Line
	6400 3000 6100 3000
Wire Wire Line
	6100 3000 6100 2200
Connection ~ 6100 2200
Wire Wire Line
	6700 3500 6300 3500
Connection ~ 6300 3500
Wire Wire Line
	6300 3500 6300 4150
NoConn ~ 7200 3200
$Comp
L dk_Logic-Shift-Registers:SN74HC595N U8
U 1 1 5EFAB6D7
P 7000 4350
F 0 "U8" H 6550 3800 60  0000 C CNN
F 1 "SN74HC595N" H 7100 5000 60  0000 C CNN
F 2 "digikey-footprints:DIP-16_W7.62mm" H 7200 4550 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74hc595" H 7200 4650 60  0001 L CNN
F 4 "296-1600-5-ND" H 7200 4750 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74HC595N" H 7200 4850 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 7200 4950 60  0001 L CNN "Category"
F 7 "Logic - Shift Registers" H 7200 5050 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74hc595" H 7200 5150 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74HC595N/296-1600-5-ND/277246" H 7200 5250 60  0001 L CNN "DK_Detail_Page"
F 10 "IC 8-BIT SHIFT REGISTER 16-DIP" H 7200 5350 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 7200 5450 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7200 5550 60  0001 L CNN "Status"
	1    7000 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 3650 6100 3650
Wire Wire Line
	6100 3650 6100 3000
Connection ~ 6100 3000
Wire Wire Line
	7200 3300 7200 3600
Wire Wire Line
	7200 3600 6200 3600
Wire Wire Line
	6200 3600 6200 4050
Wire Wire Line
	6200 4050 6400 4050
$Comp
L Display_Character:KCSA02-136 U10
U 1 1 5EFBD3AE
P 7800 4250
F 0 "U10" V 8050 4700 50  0000 C CNN
F 1 "KCSA02-136" H 7800 4826 50  0001 C CNN
F 2 "Display_7Segment:KCSC02-136" H 7800 3650 50  0001 C CNN
F 3 "http://www.kingbright.com/attachments/file/psearch/000/00/00/KCSA02-136(Ver.7B).pdf" H 7300 4725 50  0001 L CNN
	1    7800 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 3950 7500 3950
Wire Wire Line
	7200 4050 7500 4050
Wire Wire Line
	7200 4250 7500 4250
Wire Wire Line
	7200 4350 7500 4350
Wire Wire Line
	7500 4450 7200 4450
NoConn ~ 7500 4650
$Comp
L power:VCC #PWR014
U 1 1 5EFBD3BE
P 8300 4250
F 0 "#PWR014" H 8300 4100 50  0001 C CNN
F 1 "VCC" H 8317 4423 50  0000 C CNN
F 2 "" H 8300 4250 50  0001 C CNN
F 3 "" H 8300 4250 50  0001 C CNN
	1    8300 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R19
U 1 1 5EFBD3C8
P 8300 4400
F 0 "R19" V 8300 4350 50  0000 L CNN
F 1 "180" V 8400 4300 50  0000 L CNN
F 2 "" V 8230 4400 50  0001 C CNN
F 3 "~" H 8300 4400 50  0001 C CNN
	1    8300 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 4550 8300 4650
Wire Wire Line
	8300 4650 8100 4650
Wire Wire Line
	8100 4550 8300 4550
Connection ~ 8300 4550
Wire Wire Line
	7200 4150 7500 4150
Wire Wire Line
	7200 4550 7500 4550
Wire Wire Line
	6400 4150 6300 4150
Connection ~ 6300 4150
Text Label 5850 4250 2    50   ~ 0
LATCH_OUT
Wire Wire Line
	6400 4250 5850 4250
Text Label 5850 4350 2    50   ~ 0
CLOCK
Wire Wire Line
	6400 4350 5850 4350
Wire Wire Line
	6400 4450 6100 4450
Wire Wire Line
	6100 4450 6100 3650
Connection ~ 6100 3650
Wire Wire Line
	6700 4950 6300 4950
Wire Wire Line
	6300 4150 6300 4950
Connection ~ 6300 4950
Wire Wire Line
	6300 4950 6300 5600
$Comp
L dk_Logic-Shift-Registers:SN74HC595N U9
U 1 1 5EFF5A85
P 7000 5800
F 0 "U9" H 6550 5250 60  0000 C CNN
F 1 "SN74HC595N" H 7100 6450 60  0000 C CNN
F 2 "digikey-footprints:DIP-16_W7.62mm" H 7200 6000 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74hc595" H 7200 6100 60  0001 L CNN
F 4 "296-1600-5-ND" H 7200 6200 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74HC595N" H 7200 6300 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 7200 6400 60  0001 L CNN "Category"
F 7 "Logic - Shift Registers" H 7200 6500 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74hc595" H 7200 6600 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74HC595N/296-1600-5-ND/277246" H 7200 6700 60  0001 L CNN "DK_Detail_Page"
F 10 "IC 8-BIT SHIFT REGISTER 16-DIP" H 7200 6800 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 7200 6900 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7200 7000 60  0001 L CNN "Status"
	1    7000 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 5100 6100 5100
Wire Wire Line
	6100 5100 6100 4450
Connection ~ 6100 4450
NoConn ~ 7200 4650
Wire Wire Line
	7200 4750 7200 5050
Wire Wire Line
	7200 5050 6150 5050
Wire Wire Line
	6150 5050 6150 5500
Wire Wire Line
	6150 5500 6400 5500
$Comp
L Display_Character:KCSA02-136 U11
U 1 1 5F0144CA
P 7800 5700
F 0 "U11" V 8050 6150 50  0000 C CNN
F 1 "KCSA02-136" H 7800 6276 50  0001 C CNN
F 2 "Display_7Segment:KCSC02-136" H 7800 5100 50  0001 C CNN
F 3 "http://www.kingbright.com/attachments/file/psearch/000/00/00/KCSA02-136(Ver.7B).pdf" H 7300 6175 50  0001 L CNN
	1    7800 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 5400 7500 5400
Wire Wire Line
	7200 5500 7500 5500
Wire Wire Line
	7200 5700 7500 5700
Wire Wire Line
	7200 5800 7500 5800
Wire Wire Line
	7500 5900 7200 5900
NoConn ~ 7500 6100
$Comp
L power:VCC #PWR015
U 1 1 5F0144DA
P 8300 5700
F 0 "#PWR015" H 8300 5550 50  0001 C CNN
F 1 "VCC" H 8317 5873 50  0000 C CNN
F 2 "" H 8300 5700 50  0001 C CNN
F 3 "" H 8300 5700 50  0001 C CNN
	1    8300 5700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R20
U 1 1 5F0144E4
P 8300 5850
F 0 "R20" V 8300 5800 50  0000 L CNN
F 1 "180" V 8400 5750 50  0000 L CNN
F 2 "" V 8230 5850 50  0001 C CNN
F 3 "~" H 8300 5850 50  0001 C CNN
	1    8300 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 6000 8300 6100
Wire Wire Line
	8300 6100 8100 6100
Wire Wire Line
	8100 6000 8300 6000
Connection ~ 8300 6000
Wire Wire Line
	7200 5600 7500 5600
Wire Wire Line
	7200 6000 7500 6000
Text Label 5850 5700 2    50   ~ 0
LATCH_OUT
Wire Wire Line
	6400 5700 5850 5700
Text Label 5850 5800 2    50   ~ 0
CLOCK
Wire Wire Line
	6400 5800 5850 5800
Wire Wire Line
	6400 5600 6300 5600
Connection ~ 6300 5600
Wire Wire Line
	6300 5600 6300 6400
Wire Wire Line
	6400 5900 6100 5900
Wire Wire Line
	6100 5900 6100 5100
Connection ~ 6100 5100
Wire Wire Line
	6700 6400 6300 6400
Connection ~ 6300 6400
Wire Wire Line
	6300 6400 6300 6550
NoConn ~ 7200 6100
NoConn ~ 7200 6200
$Comp
L Device:R_Network08 RN1
U 1 1 5EE63AEE
P 1950 2400
F 0 "RN1" H 2338 2446 50  0000 L CNN
F 1 "10K" H 2338 2355 50  0000 L CNN
F 2 "Resistor_THT:R_Array_SIP9" V 2425 2400 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 1950 2400 50  0001 C CNN
	1    1950 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 3200 1850 3200
Wire Wire Line
	1500 3100 1750 3100
Wire Wire Line
	1500 2900 1550 2900
Wire Wire Line
	1500 3600 2250 3600
Wire Wire Line
	1500 3500 2150 3500
Wire Wire Line
	1500 3400 2050 3400
Wire Wire Line
	1500 3300 1950 3300
Wire Wire Line
	2650 4100 2650 4400
Wire Wire Line
	2250 2600 2250 3600
Connection ~ 2250 3600
Wire Wire Line
	2250 3600 2800 3600
Wire Wire Line
	2150 2600 2150 3500
Connection ~ 2150 3500
Wire Wire Line
	2150 3500 2800 3500
Wire Wire Line
	2050 2600 2050 3400
Connection ~ 2050 3400
Wire Wire Line
	2050 3400 2800 3400
Wire Wire Line
	1950 2600 1950 3300
Connection ~ 1950 3300
Wire Wire Line
	1950 3300 2800 3300
Wire Wire Line
	1850 2600 1850 3200
Connection ~ 1850 3200
Wire Wire Line
	1850 3200 2800 3200
Wire Wire Line
	1750 2600 1750 3100
Connection ~ 1750 3100
Wire Wire Line
	1750 3100 2800 3100
Wire Wire Line
	1650 2600 1650 3000
Wire Wire Line
	1500 3000 1650 3000
Connection ~ 1650 3000
Wire Wire Line
	1650 3000 2800 3000
Wire Wire Line
	1550 2600 1550 2900
Connection ~ 1550 2900
Wire Wire Line
	1550 2900 2800 2900
Wire Wire Line
	1550 2200 1550 2150
Wire Wire Line
	1550 2150 1250 2150
Wire Wire Line
	1250 2150 1250 2200
$Comp
L Device:R_Network08 RN2
U 1 1 5EEDB287
P 1950 4850
F 0 "RN2" H 2338 4896 50  0000 L CNN
F 1 "10K" H 2338 4805 50  0000 L CNN
F 2 "Resistor_THT:R_Array_SIP9" V 2425 4850 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 1950 4850 50  0001 C CNN
	1    1950 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 5650 1850 5650
Wire Wire Line
	1550 5550 1750 5550
Wire Wire Line
	1550 5450 1650 5450
Wire Wire Line
	1550 5350 2850 5350
Wire Wire Line
	1550 6050 2250 6050
Wire Wire Line
	1550 5950 2150 5950
Wire Wire Line
	1550 5850 2050 5850
Wire Wire Line
	1550 5750 1950 5750
Wire Wire Line
	2250 5050 2250 6050
Connection ~ 2250 6050
Wire Wire Line
	2250 6050 2850 6050
Wire Wire Line
	2150 5050 2150 5950
Connection ~ 2150 5950
Wire Wire Line
	2150 5950 2850 5950
Wire Wire Line
	2050 5050 2050 5850
Connection ~ 2050 5850
Wire Wire Line
	2050 5850 2850 5850
Wire Wire Line
	1950 5050 1950 5750
Connection ~ 1950 5750
Wire Wire Line
	1950 5750 2850 5750
Wire Wire Line
	1850 5050 1850 5650
Connection ~ 1850 5650
Wire Wire Line
	1850 5650 2850 5650
Wire Wire Line
	1750 5050 1750 5550
Connection ~ 1750 5550
Wire Wire Line
	1750 5550 2850 5550
Wire Wire Line
	1650 5050 1650 5450
Connection ~ 1650 5450
Wire Wire Line
	1650 5450 2850 5450
Wire Wire Line
	1550 5050 1550 5350
Connection ~ 1550 5350
Wire Wire Line
	1550 4650 1550 4550
Wire Wire Line
	1550 4550 1250 4550
Wire Wire Line
	1250 4550 1250 4650
$EndSCHEMATC
